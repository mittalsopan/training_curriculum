package com.sopan.assigment.trainingCurriculum.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sopan.assigment.trainingCurriculum.model.TrainingSession;

@Repository
public interface TrainingSessionRepository extends CrudRepository<TrainingSession,Integer> {

}
