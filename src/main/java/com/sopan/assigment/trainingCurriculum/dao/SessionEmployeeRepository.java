package com.sopan.assigment.trainingCurriculum.dao;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sopan.assigment.trainingCurriculum.model.SessionEmployee;

@Repository
public interface SessionEmployeeRepository extends CrudRepository<SessionEmployee,Integer> {

	public List<SessionEmployee> findByTrainingSessionSessionId(int id); 
	public List<SessionEmployee> findByEmployeeEmployeeId(int id); 
}
