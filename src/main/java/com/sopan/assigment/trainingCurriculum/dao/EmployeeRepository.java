package com.sopan.assigment.trainingCurriculum.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sopan.assigment.trainingCurriculum.model.Employee;

@Repository
public interface EmployeeRepository extends CrudRepository<Employee,Integer>  {

	public Employee findByemployeeEmail(String employeeEmail);

}
