package com.sopan.assigment.trainingCurriculum.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sopan.assigment.trainingCurriculum.model.TrainingSession;
import com.sopan.assigment.trainingCurriculum.service.AuthorizationService;
import com.sopan.assigment.trainingCurriculum.service.TrainingSessionService;


//@RestController
@Controller
public class TrainingSessionController {
	
	@Autowired
	private TrainingSessionService trainingSessionService;
	
	@Autowired
	private AuthorizationService authorizationService;

	@RequestMapping(method=RequestMethod.GET, value="/trainer/trainingsessions")
	public String getAllTrainingSession(Map<String, Object> model, HttpSession session, HttpServletResponse response) {
		if(authorizationService.isLogin(session) && authorizationService.isAdmin(session)) {
		model.put("trainingSessions", trainingSessionService.getAllTrainingSession());
		} else {
					try {
						model.put("message-notAdmin", "alert-danger");	
						response.sendRedirect("/logout");
					} catch (IOException e) {
						e.printStackTrace();
					}
		}
		return "trainingSessions";
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/trainer/addtrainingsessions")
	public String AddTrainingSession(Map<String, Object> model, HttpSession session, HttpServletResponse response) {
		if(authorizationService.isLogin(session) && authorizationService.isAdmin(session)) {
			
		} else {
				try {
					model.put("message-notAdmin", "alert-danger");	
					response.sendRedirect("/logout");
				} catch (IOException e) {
					e.printStackTrace();
				}
	}
		return "addTrainingSessions";
	}

	@RequestMapping(method=RequestMethod.POST, value="/trainer/addtrainingsessions" 
			, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public void addTrainingSession(TrainingSession trainingSession, HttpServletResponse response, Map<String, Object> model, HttpSession session) {
		if(authorizationService.isLogin(session) && authorizationService.isAdmin(session)) {
		trainingSessionService.addTrainingSession(trainingSession);
		try {
			response.sendRedirect("/trainer/trainingsessions");
		} catch (IOException e) {
			e.printStackTrace();
		}
		} else {
			try {
				model.put("message-notAdmin", "alert-danger");	
				response.sendRedirect("/logout");
			} catch (IOException e) {
				e.printStackTrace();
			}
}
	}

	@RequestMapping(method=RequestMethod.GET, value="/trainer/edittrainingsessions/{id}")
	public String editTrainingSession(@PathVariable(value="id") int id, Map<String, Object> model, HttpServletResponse response, HttpSession session) {	
		if(authorizationService.isLogin(session) && authorizationService.isAdmin(session)) {
			
		} else {
			try {
				model.put("message-notAdmin", "alert-danger");	
				response.sendRedirect("/logout");
			} catch (IOException e) {
				e.printStackTrace();
			}
}
		model.put("trainingSession", trainingSessionService.editTrainingSession(id));
		return "editTrainingSessions";
	}
	
	
	@RequestMapping(method=RequestMethod.POST, value="/trainer/edittrainingsessions/{id}" 
			, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public void editTrainingSession(@PathVariable(value="id") int id, TrainingSession trainingSession, HttpServletResponse response) {
		trainingSessionService.editTrainingSession(trainingSession, id);
		try {
			response.sendRedirect("/trainer/trainingsessions");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(method=RequestMethod.GET, value="/trainer/deletetrainingsessions/{id}")
	public void AddTrainingSession(@PathVariable(value="id") int id, HttpServletResponse response) {
		trainingSessionService.deleteTrainingSession(id);
		try {
			response.sendRedirect("/trainer/trainingsessions");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}