package com.sopan.assigment.trainingCurriculum.controller;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.sopan.assigment.trainingCurriculum.model.SessionEmployee;
import com.sopan.assigment.trainingCurriculum.service.AuthorizationService;
import com.sopan.assigment.trainingCurriculum.service.SessionEmployeeService;


@Controller
public class SessionEmployeeController {


	@Autowired
	private SessionEmployeeService sessionEmployeeService;
	
	@Autowired
	private AuthorizationService authorizationService;

	@RequestMapping(method=RequestMethod.GET, value="/trainer")
	public String getAllTrainingSession(Map<String, Object> model, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
		if(authorizationService.isLogin(session)) {
			System.out.println(session.getAttribute("sessionEmployeeEmail"));
			System.out.println(session.getAttribute("sessionEmployeeEmail"));
			model.put("employeeEmail", session.getAttribute("sessionEmployeeEmail"));
			model.put("sessionLogedin", session.getAttribute("sessionLogedin"));
			model.put("trainingSessions", sessionEmployeeService.getAllTrainingSession());
		} else {
			try {
				model.put("message-error", "alert-danger");
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return "sessionsEmployee";
	}

	@RequestMapping(method=RequestMethod.GET, value="/trainer/viewsessionemployee/{id}")
	public String viewSessionEmployee(Map<String, Object> model, @PathVariable(value="id") int id,  HttpServletResponse response, HttpSession session) {
		if(authorizationService.isLogin(session)) {
		model.put("sessionId", id);
		model.put("sessionEmployeeId", (int)session.getAttribute("sessionEmployeeId"));
		model.put("employesSession", sessionEmployeeService.viewSessionEmployee(id));
		} else {
			try {
				model.put("message-error", "alert-danger");
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return "viewSessionEmployee";
	}

	@RequestMapping(method=RequestMethod.POST, value="/trainer/addsessionemployee/{id}" 
			, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public void addSessionEmployee(Map<String, Object> model, SessionEmployee sessionEmployee, HttpServletResponse response, @PathVariable(value="id") int id, HttpSession session) {
		if(authorizationService.isLogin(session)) {
		sessionEmployeeService.addSessionEmployee(sessionEmployee, session, id);
		try {
			response.sendRedirect("/trainer/viewsessionemployee/" + id);
		} catch (IOException e) {
			e.printStackTrace();
		}
		} else {
			try {
				model.put("message-error", "alert-danger");
				response.sendRedirect("/login");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
