package com.sopan.assigment.trainingCurriculum.controller;

import java.io.IOException;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import com.sopan.assigment.trainingCurriculum.model.Employee;
import com.sopan.assigment.trainingCurriculum.service.EmployeeService;

@Controller
public class EmployeeController {

	@Autowired
	EmployeeService employeeService;


	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String register() {
		return "registration";
	}

	@RequestMapping(method=RequestMethod.POST, value="/register" 
			, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public String processRegister(Employee employee, Map<String, Object> model) {
		if(employeeService.checkUserExist(employee)) {
			employeeService.processRegister(employee);
		} else {
			model.put("message-type", "alert-danger");
		}
		return "registration";
	}
	
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(HttpSession session, HttpServletResponse response) {
//		if(session.getAttribute("sessionLogedin") != null && session.getAttribute("sessionEmployeeRole") == "admin" ) {
//			try {
//				response.sendRedirect("/trainer/trainingsessions");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		} else {
//			try {
//				response.sendRedirect("/trainer");
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
//		}
		return "login";
	}
	
	@RequestMapping(method=RequestMethod.POST, value="/login" 
			, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, 
			        produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
	public String processlogin(Employee employee, Map<String, Object> model, HttpSession session, HttpServletResponse response, HttpServletRequest request) {
		if(employeeService.loginService(employee)) {
			Employee logedinEmployee = employeeService.getEmployeeByEmail(employee);
			request.getSession().setAttribute("sessionLogedin", true);
			request.getSession().setAttribute("sessionEmployeeId", logedinEmployee.getEmployeeId());
			request.getSession().setAttribute("sessionEmployeeEmail", logedinEmployee.getEmployeeEmail());
			request.getSession().setAttribute("sessionEmployeeRole", logedinEmployee.getEmployeeRole());
			System.out.println(session.getAttribute("sessionEmployeeRole") );
			if(session.getAttribute("sessionEmployeeRole").equals("admin")) {
				try {
					response.sendRedirect("/trainer/trainingsessions");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else {
				try {
					response.sendRedirect("/trainer");
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} else {
			model.put("message-error", "alert-danger");
		}
		return "login";
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public void logut(HttpSession session, HttpServletResponse response) {
		session.invalidate();
		try {
			response.sendRedirect("/login");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
