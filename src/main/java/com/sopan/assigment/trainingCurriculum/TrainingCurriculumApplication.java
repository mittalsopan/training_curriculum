package com.sopan.assigment.trainingCurriculum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import com.sopan.assigment.trainingCurriculum.controller.TrainingSessionController;

@SpringBootApplication
public class TrainingCurriculumApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainingCurriculumApplication.class, args);
	}
}
