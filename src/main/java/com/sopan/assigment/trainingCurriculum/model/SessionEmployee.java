package com.sopan.assigment.trainingCurriculum.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="session_employee", schema ="trainingCurriculum")
public class SessionEmployee implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "record_id")
	private int recordId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "sessionId",  insertable=true, updatable=true)
	private TrainingSession trainingSession;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "employeeId",  insertable=true, updatable=true)
	private Employee employee;

	@Column(name= "e_name")
	private String eName;

	public SessionEmployee() {
		
	}
	
	public SessionEmployee(int recordId, int sessionId, int employeeId, String eName) {
		super();
		this.recordId = recordId;
		this.employee = new Employee(employeeId, "", "", "");
		this.trainingSession = new TrainingSession(sessionId, "", "", "", "", "", "", "");
		this.eName = eName;
	}

	public int getRecordId() {
		return recordId;
	}

	public void setRecordId(int recordId) {
		this.recordId = recordId;
	}

	public TrainingSession getTrainingSession() {
		return trainingSession;
	}

	public void setTrainingSession(TrainingSession trainingSession) {
		this.trainingSession = trainingSession;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public String geteName() {
		return eName;
	}

	public void seteName(String eName) {
		this.eName = eName;
	}

	

	
}
