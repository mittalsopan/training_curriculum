package com.sopan.assigment.trainingCurriculum.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="employee", schema ="trainingCurriculum")
public class Employee implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "employee_id")
	private int employeeId;

	@Column(name= "employee_email")
	private String employeeEmail;
	
	@Column(name= "employee_password")
	private String employeePassword;
	
	@Column(name= "employee_role", columnDefinition="default 'User'")
	private String employeeRole;

	
	
	public Employee() {
		
	}

	public Employee(int userId, String employeeEmail, String employeePassword, String employeeRole) {
		super();
		this.employeeId = userId;
		this.employeeEmail = employeeEmail;
		this.employeePassword = employeePassword;
		this.employeeRole = employeeRole;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int userId) {
		this.employeeId = userId;
	}

	public String getEmployeeEmail() {
		return employeeEmail;
	}

	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}

	public String getEmployeePassword() {
		return employeePassword;
	}

	public void setEmployeePassword(String employeePassword) {
		this.employeePassword = employeePassword;
	}

	public String getEmployeeRole() {
		return employeeRole;
	}

	public void setEmployeeRole(String employeeRole) {
		this.employeeRole = employeeRole;
	}
	
	

}