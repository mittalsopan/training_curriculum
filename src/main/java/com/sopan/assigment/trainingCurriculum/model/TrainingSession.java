package com.sopan.assigment.trainingCurriculum.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Column;

@Entity
@Table(name="training_session")
public class TrainingSession implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name= "session_id")
	private int sessionId;
	
	@Column(name= "topic")
	private String topic;
	
	@Column(name= "description")
	private String description;
	
	@Column(name= "start_time")
	private String startTime;
	
	@Column(name= "end_time")
	private String endTime;
	
	@Column(name= "start_date")
	private String startDate;
	
	@Column(name= "end_date")
	private String endDate;
	
	@Column(name= "trainer")
	private String trainer;
	
//	@ManyToOne(mappedBy="trainingSession")
//	List<SessionEmployee> sessionEmployee;
	
	public TrainingSession() {

	}
	
	public TrainingSession(int sessionId, String topic, String description, String startTime, String endTime, String startDate, String endDate, String trainer) {
		super();
		this.topic = topic;
		this.description = description;
		this.startTime = startTime;
		this.endTime = endTime;
		this.startDate = startDate;
		this.endDate = endDate;
		this.trainer = trainer;
	}
	

	public int getSessionId() {
		return sessionId;
	}

	public void setSessionId(int sessionId) {
		this.sessionId = sessionId;
	}

	public String getTopic() {
		return topic;
	}

	public void setTopic(String topic) {
		this.topic = topic;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getTrainer() {
		return trainer;
	}

	public void setTrainer(String trainer) {
		this.trainer = trainer;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
