package com.sopan.assigment.trainingCurriculum.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sopan.assigment.trainingCurriculum.dao.SessionEmployeeRepository;
import com.sopan.assigment.trainingCurriculum.dao.TrainingSessionRepository;
import com.sopan.assigment.trainingCurriculum.model.TrainingSession;
import com.sopan.assigment.trainingCurriculum.model.SessionEmployee;


@Service
public class TrainingSessionService {
	
	@Autowired
	private TrainingSessionRepository trainingSessionRepository;
	
	@Autowired
	private SessionEmployeeRepository sessionEmployeeRepository;
	
	public Iterable<TrainingSession> getAllTrainingSession(){
			return trainingSessionRepository.findAll();
		}

	public void addTrainingSession(TrainingSession trainingSession) {
		trainingSessionRepository.save(trainingSession);
		
	}

	public TrainingSession editTrainingSession(int id) {
		TrainingSession trainingSession = trainingSessionRepository.findById(id).get();
		//System.out.println(trainingSession );
		return trainingSession ;
	}

	public void editTrainingSession(TrainingSession trainingSession, int id) {
		TrainingSession TrainingSessionToUpdate	 = trainingSessionRepository.findById(id).get();
		TrainingSessionToUpdate.setTopic(trainingSession.getTopic());
		System.out.println(trainingSession.getTopic());
		TrainingSessionToUpdate.setDescription(trainingSession.getDescription());
		TrainingSessionToUpdate.setEndDate(trainingSession.getEndDate());
		TrainingSessionToUpdate.setEndTime(trainingSession.getEndTime());
		TrainingSessionToUpdate.setStartDate(trainingSession.getStartDate());
		TrainingSessionToUpdate.setStartTime(trainingSession.getStartTime());
		TrainingSessionToUpdate.setTrainer(trainingSession.getTrainer());
		trainingSessionRepository.save(TrainingSessionToUpdate);
		
	}

	public void deleteTrainingSession(int id) {
		List<SessionEmployee> topics = new ArrayList<>();
		sessionEmployeeRepository.findByTrainingSessionSessionId(id).forEach(topics::add);
		for(int i=0;i<topics.size();i++) {
			sessionEmployeeRepository.delete(topics.get(i));
		}
		TrainingSession trainingSession = trainingSessionRepository.findById(id).get();
		trainingSessionRepository.delete(trainingSession);
	}

}
