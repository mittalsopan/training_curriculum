package com.sopan.assigment.trainingCurriculum.service;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sopan.assigment.trainingCurriculum.dao.EmployeeRepository;

@Service
public class AuthorizationService {
	
	@Autowired
	private EmployeeRepository employeeRepository;

	public boolean isLogin(HttpSession session) {
		if(session.getAttribute("sessionLogedin") != null && (boolean) session.getAttribute("sessionLogedin")) {
			return true;
		}
		return false;
	}
	
	public boolean isAdmin(HttpSession session) {
		if(session.getAttribute("sessionEmployeeRole") != null && session.getAttribute("sessionEmployeeRole").equals("admin")) {
			return true;
		}
		return false;
	}
	
}
