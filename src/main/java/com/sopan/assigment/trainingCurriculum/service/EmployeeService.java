package com.sopan.assigment.trainingCurriculum.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

import com.sopan.assigment.trainingCurriculum.dao.EmployeeRepository;
import com.sopan.assigment.trainingCurriculum.model.Employee;

@Service
public class EmployeeService {
	
	static String hash = "$2a$06$.rCVZVOThsIa97pEDOxvGuRRgzG64bvtJ0938xuqzv18d3ZpQhstC";	 
	
	@Autowired
	private EmployeeRepository employeeRepository;
	
	public void processRegister(Employee employee) {
		String email = employee.getEmployeeEmail();
		email = email.concat("@thoughtclan.com");
		employee.setEmployeeEmail(email);
		String password = employee.getEmployeePassword();
		String hashedPassword = hashPassword(password);
		employee.setEmployeePassword(hashedPassword);
		employee.setEmployeeRole("user");
		employeeRepository.save(employee);
		//HttpSession.setAttribute("login", "true");
	}
	
	public Boolean checkUserExist(Employee employee) {
		String userEmail = employee.getEmployeeEmail();
		userEmail = userEmail.concat("@thoughtclan.com");
		Employee foundEmployee = employeeRepository.findByemployeeEmail(userEmail);
		if(foundEmployee == null) {
			return true;
		}
		
		return false;
	}
	
public boolean loginService(Employee employee) {
	System.out.println("Inside Service");;
	String userEmail = employee.getEmployeeEmail();
	userEmail = userEmail.concat("@thoughtclan.com");
	Employee foundEmployee = employeeRepository.findByemployeeEmail(userEmail);
	if(foundEmployee != null && checkPassword(employee.getEmployeePassword(), foundEmployee.getEmployeePassword())) {
		return true;
	}
		return false;
	}

	public Employee getEmployeeByEmail(Employee employee) {
		String userEmail = employee.getEmployeeEmail();
		userEmail = userEmail.concat("@thoughtclan.com");
		Employee foundEmployee = employeeRepository.findByemployeeEmail(userEmail);
		return foundEmployee;
	}

/*
 *     BCRYPT Related Stuf
 */
	public static String hashPassword(String password_plaintext) {
		String hashed_password = BCrypt.hashpw(password_plaintext, hash);
		return(hashed_password);
	}
	
	public static boolean checkPassword(String password_plaintext, String stored_hash) {
		boolean password_verified = false;
		password_verified = BCrypt.checkpw(password_plaintext, stored_hash);
		return(password_verified);
	}
	

}
