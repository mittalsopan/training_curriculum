package com.sopan.assigment.trainingCurriculum.service;


import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sopan.assigment.trainingCurriculum.dao.EmployeeRepository;
import com.sopan.assigment.trainingCurriculum.dao.SessionEmployeeRepository;
import com.sopan.assigment.trainingCurriculum.dao.TrainingSessionRepository;
import com.sopan.assigment.trainingCurriculum.model.Employee;
import com.sopan.assigment.trainingCurriculum.model.SessionEmployee;
import com.sopan.assigment.trainingCurriculum.model.TrainingSession;



@Service
public class SessionEmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private SessionEmployeeRepository sessionEmployeeRepository;
	
	@Autowired
	private TrainingSessionRepository trainingSessionRepository;
	
	
	public Iterable<TrainingSession> getAllTrainingSession(){
			return trainingSessionRepository.findAll();
		}

	public Iterable<SessionEmployee> viewSessionEmployee(int id){
		return sessionEmployeeRepository.findByTrainingSessionSessionId(id);
		}

	public void addSessionEmployee(SessionEmployee sessionEmployee, HttpSession session, int id) {
		int EmployeeId = (int) session.getAttribute("sessionEmployeeId");
		
		Employee employee = employeeRepository.findById(EmployeeId).get();
		String eName = (String) session.getAttribute("sessionEmployeeEmail");
		sessionEmployee.setEmployee(employee);
		sessionEmployee.seteName(eName);
		
		TrainingSession trainingSession = trainingSessionRepository.findById(id).get();
		//String eName = (String) session.getAttribute("sessionEmployeeEmail");
		sessionEmployee.setTrainingSession(trainingSession);
		//sessionEmployee.seteName(eName);
		
		sessionEmployeeRepository.save(sessionEmployee);
		
	}

}
